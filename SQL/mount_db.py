import random
import string
from utils.database import Database
from dotenv import load_dotenv

load_dotenv()

alphabet = string.ascii_lowercase
db = Database()

def create_table():

    db.execute("""
        CREATE TABLE users (
            id_user_pk SERIAL PRIMARY KEY,
            name TEXT NOT NULL,
            age INTEGER NOT NULL
        )
    """)

def create_table_2():

    db.execute("""
        CREATE TABLE composto (
            a INTEGER NOT NULL,
            b INTEGER NOT NULL,
            c INTEGER NOT NULL
        )
    """)

    db.execute("create index index_a on composto(a)")
    db.execute("create index index_b on composto(b)")

def fill_table():
    for i in range(3000):
        rows = []
        for i in range(3000):
            name = ''.join((random.choice(alphabet) for i in range(4)))
            age = random.randint(1,100)
            rows.append({'name': name, 'age': age})
        db.executemany("""
            INSERT INTO users (name, age) values (%(name)s, %(age)s)
        """, rows)

def fill_table_2():
    for i in range(3000):
        rows = []
        for i in range(3000):
            a = random.randint(1,100)
            b = random.randint(1,100)
            c = random.randint(1,100)
            rows.append({'a': a, 'b': b, 'c': c})
        db.executemany("""
            INSERT INTO composto (a, b, c) values (%(a)s, %(b)s, %(c)s)
        """, rows)