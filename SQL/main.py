from dotenv import load_dotenv
from utils.database import Database
import pandas as pd

load_dotenv()

db = Database()

select_1 = db.select("""select * from users where id_user_pk < 4""")
select_2 = db.select("""select * from users where id_user_pk < %(id)s""", {'id': 4})

select_3 = db.pd_select("""select * from users where id_user_pk < 4""")
select_4 = db.pd_select("""select * from users where id_user_pk < %(id)s""", {'id': 4})

db.execute("""INSERT INTO users (name, age) VALUES ('abc', 12)""")
db.execute("""INSERT INTO users (name, age) VALUES (%(name)s, %(age)s)""", {'name': 'xsd', 'age': 45})

values_list = [{'name': 'xsd', 'age': 45}, {'name': 'adsfs', 'age': 23}]
df = pd.DataFrame([{'name': 'daaf', 'age': 22}, {'name': 'adf', 'age': 55}])

db.executemany("""INSERT INTO users (name, age) VALUES (%(name)s, %(age)s)""", values_list)
db.pd_executemany("""INSERT INTO users (name, age) VALUES (%(name)s, %(age)s)""", df)
