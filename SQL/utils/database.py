from contextlib import contextmanager
from collections import namedtuple
from typing import Type, List, Dict
from os import getenv
import psycopg2.pool
import pandas as pd


class Database:
    """Cria um objeto para efetuar a interação com o banco de dados
    
    [Args]
        min_connections: Especifíca o número mínimo de conexões que o pool de conexões terá
        max_connections: Especifíca o número máximo de conexões que o pool de conexões terá
    """

    def __init__(self, min_connections: int = 2, max_connections: int = 20):
        self.min_connections = min_connections
        self.max_connections = max_connections
        self.Connection = namedtuple('Connection', ['connection', 'cursor'])
        self.__create_connection_pool()

    def __create_connection_pool(self):
        """Cria o pool de conexões"""

        try:
            connection_params = {
                'minconn': self.min_connections,
                'maxconn': self.max_connections,
                'host': getenv('DB_HOST'),
                'port': getenv('DB_PORT'),
                'dbname': getenv('DB_DBNAME'),
                'user': getenv('DB_USER'),
                'password': getenv('DB_PASSWORD')
            }
            self.connection_pool = psycopg2.pool.SimpleConnectionPool(**connection_params)
        except Exception as e:
            print(f'Falha ao conectar com o banco de dados: {e}')
        
    @contextmanager
    def __get_connection(self, itersize: int):
        """Gerenciador de contexto que abre puxando uma conexão do pull e criando um cursor, e encerra fechando o cursor e devolvendo a conexão para o pool
        
        Args:
            itersize: Especifica o número de linhas a serem buscadas em uma única ida ao banco
        """

        try:
            conn = self.connection_pool.getconn()
            cursor = conn.cursor()
            cursor.itersize = itersize
            yield self.Connection(conn, cursor)
        finally:
            cursor.close()
            self.connection_pool.putconn(conn)

    def select(self, select_string: str, params: Dict = {}, itersize: int = 2000) -> List[Dict]:
        """efetua uma operação de select no banco
        
        [Args]
            select_string: string que contém o select
                - "select * from a where id = %(id)s and x = %(x)s"
            params: dicionário com os valores para entrarem no select
                - {'id': 15, 'x': 14}
            itersize: Especifica o número de linhas a serem buscadas em uma única ida ao banco
        
        [Return]
            [dict]: Retorna uma lista de dicionários, sendo cada chave do dicionário o nome da coluna sql, e ela estará em UPPERCASE
                - [{'A': 45, 'B': 15}, {'A': 88, 'B': 55}]
        """

        with self.__get_connection(itersize) as conn:
            conn.cursor.execute(select_string, params)
            columns = [col[0].upper() for col in conn.cursor.description]
            response = [dict(zip(columns, row)) for row in conn.cursor.fetchall()]

        return response

    def pd_select(self, select_string: str, params: Dict = {}, itersize: int = 2000) -> Type[pd.DataFrame]:
        """efetua uma operação de select no banco, mas retorna um pandas dataframe
        
        [Args]
            select_string: string que contém o select
                - "select * from a where id = %(id)s and x = %(x)s"
            params: dicionário com os valores para entrarem no select
                - {'id': 15, 'x': 14}
            itersize: Especifica o número de linhas a serem buscadas em uma única ida ao banco

        [Return]
            [pd.DataFrame]: Retorna um DataFrame, sendo que o nome das colunas estará em UPPERCASE
        """

        with self.__get_connection(itersize) as conn:
            conn.cursor.execute(select_string, params)
            columns = [col[0].upper() for col in conn.cursor.description]
            response = pd.DataFrame(conn.cursor.fetchall(), columns=columns)

        return response

    def execute(self, execution_string: str, params: Dict = {}, itersize: int = 2000):
        """Efetua as operações de SQL que não sejam SELECT (ou seja, update, insert, delete...)
        
        [Args]
            execution_string: string que contém a instrução sql
                - "insert into a (id, x) values (%(id)s, %(x)s)"
            params: dicionário com os valores para entrarem na query
                - {'id': 15, 'x': 14}
            itersize: Especifica o número de linhas a serem buscadas em uma única ida ao banco
        """

        with self.__get_connection(itersize) as conn:
            conn.cursor.execute(execution_string, params)
            conn.connection.commit()

    def executemany(self, execution_string: str, values_list: List[Dict], itersize: int = 2000):
        """Efetua as operações de SQL em batch que não sejam SELECT (ou seja, update, insert, delete...)
        
        [Args]
            execution_string: string que contém a instrução sql
                - "insert into a (id, x) values (%(id)s, %(x)s)"
            values_list: lista de dicionários com os valores para entrarem na query
                - [{'id': 15, 'x': 14}, {'id': 23, 'x': 33}]
            itersize: Especifica o número de linhas a serem buscadas em uma única ida ao banco
        """

        with self.__get_connection(itersize) as conn:
            conn.cursor.executemany(execution_string, values_list)
            conn.connection.commit()

    def pd_executemany(self, execution_string: str, df: Type[pd.DataFrame], itersize: int = 2000):
        """Efetua as operações de SQL em batch que não sejam SELECT (ou seja, update, insert, delete...)
        
        [Args]
            execution_string: string que contém a instrução sql
                - "insert into a (id, x) values (%(id)s, %(x)s)"
            df: DataFrame com todas as colunas nomeadas igual aos binds da query
            itersize: Especifica o número de linhas a serem buscadas em uma única ida ao banco
        """

        with self.__get_connection(itersize) as conn:
            conn.cursor.executemany(execution_string, df.to_dict(orient='records'))
            conn.connection.commit()

    def __del__(self):
        self.connection_pool.closeall()
