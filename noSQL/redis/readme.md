É um banco NoSql, que roda totalmente em memória, com foco em ser extremamente rápido. E por ser em memória ele se encaixa perfeitamente em casos como, construção de filas e cache. Porém não é muito recomendado para dados que precisam de serem mantidos por muito tempo, ou seja, durabilidade.

O redis funciona com um esquema que cada componente dele terá uma key numérica, e um valor que pode ser uma string, um json, qualquer coisa.

E deixa como opcional adicionar durabilidade, ou seja, ele envia os dados para o HD caso a durabilidade seja adicionada.

- `Journaling`: A cada transaction os dados são jogados para o disco
- `Snapshotting`: A cada x tempo, o bando salva uma cópia no disco
- `Ambos`: As duas opções acontecem, sendo que elas ocorrem de forma assíncrona.

Como exemplo, vamos criar um banco no docker, o arquivo `docker-compose.yml` está [aqui](./noSQL/redis/docker-compose.yml), e então vamos entrar na cli dele.

```sh
$ docker exec -it nome_do_container redis-cli
```

Agora vamos criar uma key.

```sh
> set name "lucas"
```

E agora vamos ver o registro

```sh
> get name
```

Agora vamos setar uma chave com tempo de expiração de 30s, depois vamos pesquisar por ela, e depois de 30s vamos pesquisar novamente e verá que não vai ter mais nada

```sh
> set nametmp "lucasdutra" EX 30
> get nametmp
# 30s depois
> get nametmp
```

Também pode verificar se uma chave existe com o comando 

```sh
> exists key_name
```

Para deletar uma chave

```sh
> del key_name
```

Também é possível adicionar coisas ao registro

```sh
> append name "dutra"
> get name
```


- [Redis In-memory Database Crash Course](https://www.youtube.com/watch?v=sVCZo5B8ghE&list=PLQnljOFTspQXjD0HOzN7P2tgzu7scWpl2&index=7)