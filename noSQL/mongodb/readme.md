
Vamos começar com bancos noSQL, e vamos começar estudando mongoDB.

Para isso, vamos utilizar docker para subir o banco, e ele, o arquivo do docker-compose.yml está [aqui](./noSQL/mongo/docker-compose.yml).

Para trabalhar com o mongo, vamos começar com o terminal iterativo deles, para executá-lo dentro do docker, rode o comando:

```sh
$ docker exec -it nome_do_container mongo
```

Crie um banco chamado `teste` com o comando:

```sh
$ use teste
```

Agora vamos criar uma coleção, que seria análoga a uma tabela sql. A diferença aqui é que uma coleção não tem imposição de forma, ou seja, você não precisa definir os campos, não precisa definir quais são as chaves primárias, estrangeiras e etc, logo você abre mão do auxílio que o banco de da para manté-lo consistente.

Então vamos criar uma coleção chamada employees, e já vamos inserir alguns registros.

```sh
$ db.employees.insertMany([{name: "lucas"}])
```

Agora vamos criar outro registro, porém, ele não vai seguir o esquema do primeiro.

```sh
$ db.employees.insertMany([{name: "lucas", age: "26"}])
```

Agora vamos ver como isso ficou.

```sh
$ db.employees.find()
```

- [Spinning MongoDB, MongoShell and Mongo GUI with Docker](https://www.youtube.com/watch?v=DzyC8lqbjC8&list=PLQnljOFTspQXjD0HOzN7P2tgzu7scWpl2&index=4)
