# Sumário

- [**1. ACID**](#1-acid)
  - [1.1 Transactions](#11-transactions)
  - [1.2 Atomicity](#12-atomicity)
  - [1.3 Consistency](#13-consistency)
  - [1.4 Isolation](#14-isolation)
  - [1.5 Durability](#15-durability)
- [**2. Index**](#2-index)
  - [2.1 Select usando o index no where e retornando a coluna que já está presente no index](#21-select-usando-o-index-no-where-e-retornando-a-coluna-que-já-está-presente-no-index)
  - [2.2 Select usando o index no where e retornando uma coluna que não está no index](#22-select-usando-o-index-no-where-e-retornando-uma-coluna-que-não-está-no-index)
  - [2.3 Select usando a coluna fora do index para a busca](#23-select-usando-a-coluna-fora-do-index-para-a-busca)
  - [2.4 Select usando like em uma coluna para fazer a busca](#24-select-usando-like-em-uma-coluna-para-fazer-a-busca)
  - [2.5 Criando um novo index](#25-criando-um-novo-index)
  - [2.6 Repetindo os testes 2.3 e 2.4, mas agora com o index](#26-repetindo-os-testes-23-e-24-mas-agora-com-o-index)
  - [2.7 Index composto](#27-index-composto)
  - [2.9 Observações](#29-observações)
- [**Referência**](#referência)



# 1. ACID

São quatro propriedades de bancos de dados, sendo elas:
- Atomicity
- Consistency
- Isolation
- Durability

Mas primeiro vamos falar sobre transactions.

## 1.1 Transactions

A ideia da transaction é tornar um grupo de query como sendo uma única query. O motivo disso é simples. 

Imagine que você irá transferir 5 unidades de algo do usuário A para o usuário B, para isso você irá fazer a seguinte sequência de operações no seu banco de dados:

- atualizar a coluna de quantidades de A para o valor atual -5
- atualizar a coluna de quantidade de B para o valor atual +5

Mas vamos imaginar que a primeira operação ocorreu com sucesso, mas por algum motivo a segunda falhou. Ou seja, você acabou de perder 5 unidades. Elas saíram de A mas não chegaram para ninguém.

Logo o corrento a se fazer é executar esses dois processos como se fossem uma coisa só, e a operação irá se completar somente se ambas as execuções ocorrerem com sucesso.

Se tratando de um banco relacional o primeiro caso ficaria da seguinte forma:

```sql
UPDATE users
    SET qty = qty - 5
WHERE id_user = 'A';

UPDATE users
    SET qty = qty + 5
WHERE id_user = 'B';
```

E no segundo caso

```sql
BEGIN
    UPDATE users
        SET qty = qty - 5
    WHERE id_user = 'A';

    UPDATE users
        SET qty = qty + 5
    WHERE id_user = 'B';
END;
```

## 1.2 Atomicity

A ideia aqui é que a transaction seja como um átomo, ou seja, ela não pode ser dividida (na época que inventaram isso não deviam saber que dava sim pra dividir um átomo, então vida que segue...), ou seja, todas as queries dentro de uma transaction atuam como sendo uma coisa só, ou todas vão resultar em sucesso, ou todas vão resultar em falha.

Logo se uma das queries falhar, todas serão consideradas falhas, logo o commit no banco não acontece. Evitando o problema do exemplo de +5 -5.

## 1.3 Consistency

A consistência do banco de dados é algo que é garantido não pelo banco, mas sim pelos programadores. Pois a ideia de consistência é ter informações correlacionadas em diferentes lugares do banco que correspondem exatamente uma a outra. Ou seja. Se eu tenho duas tabelas, uma com nomes de pessoas que compraram um produto, e outra tabela com os produtos e com a quantidade de unidades vendidas desse produto. Logo se na tabela de produtos tivermos marcando que 100 unidades foram vendidas, a tabela de pessoas que compraram deve ter 100 pessoas (considerando que cada pessoa compra somente uma unidade).

Porém, isso não é exatamente necessário de se manter, depende de cada caso. Por exemplo, vamos imaginar o instagram, ele deve ter uma tabela com imagens postadas que contém o número de likes dessa imagem, e uma outra tabela com usuários que curtiram imagens, sendo que uma das colunas vai ser o id da imagem curtida. Teoricamente, se buscarmos todos os usuários que curtiram uma imagem específica teremos uma quantidade diferente da apresentada na tabela de imagens, afinal uma imagem com milhões de curtidas gastaria muito processamento para ser gerenciada mantendo essa consistência. Então podemos imaginar que esse processo de atualização da tabela deve ocorrer de forma assíncrona ao processo de update do número de likes, o que não travaria o processo principal, afinal, em casos de milhões de curtidas, ninguém iria conseguir se achar no meio da lista de pessoas que curtiram a imagem, então não tem problema não ser consistente em um caso desse.

## 1.4 Isolation

Quando se inicia uma transaction, os dados envolvidos devem ser "congelados", ou seja. Se você começa uma operação dentro de uma transaction, e algum outro processo efetua um update em uma das partes que você está utilizando, essa modificação deve passar desapercebida para você, pois assim que a transaction começou, ela obteve o estado das tabelas e não vai atualizá-lo até a transaction terminar.

- Isso resolve alguns problemas:
    - `Dirty read`: Imagine que duas transactions que utilizam o mesmo dado estão executando paralelamente. E em um momento da primeira transaction atualiza um valor, mas ainda não efetuou o commit, e a segunda transaction pega esse valor atualizado para uma operação, e então algo acontece na primeira transaction e ela falha, logo o valor sofrerá rollback, porém o valor já foi operado com a segunda transaction, que obteve sucesso. Logo a segunda transaction efetuou uma operação com um valor inválido, e isso foi salvo no banco.

    - `non-repeatable read`: Agora imagine que dentro de uma transaction você lê um valor, e efetua algumas operações com ele. Mas agora você precisa ler ele novamente da tabela, mas ele foi modificado por outro processo (que diferente do exemplo anterior, ocorreu com sucesso), então você obtém um valor diferente, e os seus cálculos desse ponto em diante serão feitos com um mix dos resultados obtidos anteriormente e desse novo valor. O que é errado, pois você está misturando valores atualizados com desatualizados.

    - `phantom read`: Agora você pega dados de uma tabela, processa com eles, e em quanto isso algum outro processo insere um novo dado nessa tabela, e por algum motivo a sua transaction agora vai precisar efetuar um count para pegar quantas linhas tem nessa tabela, e efetuar novas operações, mas dai vai aparecer uma linha a mais. Logo você começou processando n linhas, e terminou operando n linhas, mas como se fossem n+1.

Mas existem níveis de isolamento para resolver esses problemas.
- `read uncommitted`: Aqui a transaction pega tudo. Então não tem nenhum nível real de isolamento.
- `read committed`: Atualiza os dados dentro da transaction somente se eles forem comitados no outro processo. Resolvendo somente o problema de Dirty read.
- `repeatable read`: Não aceita atualizações de dados de forma alguma. Ou seja, os dados irão permanecer congelado do inicio ao fim da transaction. Para isso o banco versiona a tabela assim que a transaction inicia, e aquela transaction irá ler somente aquela versão da tabela. E esse nível resolve todos os problemas citados acima. 

## 1.5 Durability

É a capacidade de manter os dados assim que o commit for feito, e não ter esses dados corrompidos ou perdidos de alguma forma.


# 2. Index

Para trabalhar com index, eu fiz um docker com o postgres, e o cliente será o [omniDB](https://omnidb.org/), mas pode ser qualquer um, e um script python que insere aleatoriamente alguns milhões de registros aleatórios em uma tabela. 

- [docker-compose.yml](./SQL/docker-compose.yml)
- [python script](./SQL/mount_db.py)


Um index é uma estrutura que serve para criar "atalhos" para os registros da tabela, sendo uma estrutura bem menor e mais rápida de buscar que a própria tabela. É como se fossem aqueles divisores de páginas de dicionário, que dividem por letras, assim fica mais simples de começar a procurar.

A minha tabela terá a seguinte estrutura:

```sql
CREATE TABLE users (
    id_user_pk SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    age INTEGER NOT NULL
)
```

sendo que por padrão o postgres atribui um index na chave primária.

E agora vamos executar as queries com `explain analyze` na frente, assim vamos conseguir ver quais são os passos que o banco irá fazer para executar essa query, assim vamos conseguir ver como o index impacta nas queries.

> OBS.: Quase todo client de banco de dados possui um botão que já faz essa analise sem precisar usar explain analyze.

## 2.1 Select usando o index no where e retornando a coluna que já está presente no index

- Query
```sql
explain analyze select id_user_pk from users where id_user_pk = 2000;
```

- resultado
```sql
Index Only Scan using users_pkey on users  (cost=0.43..8.45 rows=1 width=4) (actual time=0.018..0.020 rows=1 loops=1)
  Index Cond: (id_user_pk = 2000)
  Heap Fetches: 0
Planning Time: 0.068 ms
Execution Time: 0.038 ms
```

Veja que o banco não vai na tabela, ele vai no index (`Index Only Scan`). E a busca pelo index é bem mais rápida, pois é em uma árvore binária.

E outro fator que ajuda a query ser bem mais rápida, é que o banco retorna somente o `id_user_pk`, e ele já está no index, logo a busca nunca sai do index (`Heap Fetches: 0`), pois assim que acha esse campo no index, o banco já tem tudo que precisa para me dar um retorno.

> OBS.: O planning time, é o tempo para o banco decidir o que fazer, se ele busca só no index ou se ele vai precisar ir na tabela. Já o tempo de execução, é o tempo que ele gasta executando o plano que foi estabelecido até encontrar o que foi pedido.


## 2.2 Select usando o index no where e retornando uma coluna que não está no index

> OBS.: Troque o id de busca, para evitar cache

- query
```sql
explain analyze select name from users where id_user_pk = 3000;
```

- resultado
```sql
Index Scan using users_pkey on users  (cost=0.43..8.45 rows=1 width=5) (actual time=1.480..1.484 rows=1 loops=1)
  Index Cond: (id_user_pk = 3000)
Planning Time: 0.054 ms
Execution Time: 1.502 ms
```

Veja que agora não temos mais `Index Only Scan` e sim `Index Scan`, e o tempo de execução aumentou bastante. Isso aconteceu porque o banco teve que ir na tabela para ver qual era o nome para aquele id que ele achou no index.

> OBS.: Execute a mesma query novamente e veja o cache em ação

## 2.3 Select usando a coluna fora do index para a busca

- query
```sql
explain analyze select id_user_pk from users where name = 'cdsa';
```

- resultado
```sql
Gather  (cost=1000.00..43468.38 rows=9 width=4) (actual time=4.946..267.232 rows=9 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on users  (cost=0.00..42467.48 rows=4 width=4) (actual time=11.155..261.027 rows=3 loops=3)
        Filter: (name = 'cdsa'::text)
        Rows Removed by Filter: 1333672
Planning Time: 0.057 ms
Execution Time: 267.252 ms
```

Veja que agora a situação mudou completamente. Pois como o banco não consegue efetuar a busca pelo index, ele precisa verificar a tabela propriamente dita, mas ele se aproveita que o meu pc tem múltiplos núcleos, e faz esse processo em paralelo (`Parallel Seq Scan`), mas mesmo assim, veja que essa query demora muito mais para ser executada do que as queries que buscam pelo index.

## 2.4 Select usando like em uma coluna para fazer a busca

- query
```sql
explain analyze select id_user_pk from users where name like '%ff%';
```

- Resultado
```sql
Gather  (cost=1000.00..47509.08 rows=40416 width=4) (actual time=0.227..290.040 rows=17255 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on users  (cost=0.00..42467.48 rows=16840 width=4) (actual time=0.056..281.014 rows=5752 loops=3)
        Filter: (name ~~ '%ff%'::text)
        Rows Removed by Filter: 1327923
Planning Time: 0.051 ms
Execution Time: 291.452 ms
```

Esse é definitivamente o pior método de busca, pois ele vai ter que comparar diversas permutações a cada linha do banco. Mas veja que nesse caso, não foi tão ruim assim, pois a minha tabela possui apenas 4 milhões de registros, e o campo name possui sempre 4 letras, logo não temos muitas permutações.

## 2.5 Criando um novo index

vou criar um novo index na coluna `name` (isso pode demorar um tempinho).

```sql
create index user_name on users(name);
```

## 2.6 Repetindo os testes 2.3 e 2.4, mas agora com o index

- query
```sql
explain analyze select id_user_pk from users where name = 'cdsa';
```


- resultado
```sql
Bitmap Heap Scan on users  (cost=4.50..40.06 rows=9 width=4) (actual time=0.076..0.154 rows=7 loops=1)
  Recheck Cond: (name = 'fdad'::text)
  Heap Blocks: exact=7
  ->  Bitmap Index Scan on user_name  (cost=0.00..4.50 rows=9 width=0) (actual time=0.058..0.059 rows=7 loops=1)
        Index Cond: (name = 'fdad'::text)
Planning Time: 0.202 ms
Execution Time: 0.174 ms
```

O tempo de busca melhorou muito, pois agora está efetuando um `Bitmap Index Scan`, sendo assim a busca está no index, mas ao contrário do `Index Scan` esse método trará um subset da tabela para ser analisado na condição. Isso faz com ele seja mais lento que um `Index Scan`, mas bem mais rápido que um `Parallel Seq Scan`

> OBS.: essa query só precisa trazer uma parte da tabela para ser analisada pois 7 linhas se enquadraram na condição, se a busca tivesse sido feita pelo id, e retornado o nome, teríamos tido algo bem rápido, como já foi mostrado anteriormente, pois o id é único, logo teríamos apenas uma linha da tabela para analisar


- query
```sql
explain analyze select id_user_pk from users where name like '%sa%';
```

- resultado
```sql
Gather  (cost=1000.00..43506.67 rows=400 width=4) (actual time=0.239..279.446 rows=17796 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on users  (cost=0.00..42466.67 rows=167 width=4) (actual time=0.061..269.564 rows=5932 loops=3)
        Filter: (name ~~ '%sa%'::text)
        Rows Removed by Filter: 1327743
Planning Time: 0.110 ms
Execution Time: 281.047 ms
```

Veja que o caso do like, ainda continua horrível, pois o like simplesmente desconsidera a existência do index, veja que ele continua fazendo processamento paralelo em toda a tabela.


> OBS.: Se eu tivesse buscado pelo index name, e retornado apenas o name, a busca ocorreria com `Index Only Scan`.

## 2.7 Index composto

Para essa segunda parte, vamos criar uma nova tabela:

```sql
CREATE TABLE composto (
    a INTEGER NOT NULL,
    b INTEGER NOT NULL,
    c INTEGER NOT NULL
)
```

E criar index para a e b

```sql
create index index_a on composto(a);
create index index_b on composto(b);
```

E vamos preencher essa tabela com alguns milhões de registros com o script python

### Retornando a coluna sem index, com uma pesquisa no index.

- query
```sql
explain analyze select c from composto where a = 79;
```

- resultado
```sql
Bitmap Heap Scan on composto  (cost=961.67..51801.36 rows=87901 width=4) (actual time=22.551..297.290 rows=89910 loops=1)
  Recheck Cond: (a = 79)
  Heap Blocks: exact=41122
  ->  Bitmap Index Scan on index_a  (cost=0.00..939.69 rows=87901 width=0) (actual time=13.198..13.198 rows=89910 loops=1)
        Index Cond: (a = 79)
Planning Time: 0.137 ms
Execution Time: 302.811 ms
```

Veja que a busca foi feita pelo index `index_a`, o que era esperado, já que a busca foi feita pelo `a`

### Retornando a coluna sem index, com uma pesquisa pelas duas colunas com index usando AND

- query
```sql
explain analyze select c from composto where a = 56 and b = 77;
```

- resultado
```sql
Bitmap Heap Scan on composto  (cost=2155.45..6122.62 rows=1126 width=4) (actual time=22.590..27.947 rows=912 loops=1)
  Recheck Cond: ((a = 56) AND (b = 77))
  Heap Blocks: exact=906
  ->  BitmapAnd  (cost=2155.45..2155.45 rows=1126 width=0) (actual time=22.193..22.195 rows=0 loops=1)
        ->  Bitmap Index Scan on index_a  (cost=0.00..1071.94 rows=100201 width=0) (actual time=9.984..9.984 rows=89961 loops=1)
              Index Cond: (a = 56)
        ->  Bitmap Index Scan on index_b  (cost=0.00..1082.69 rows=101101 width=0) (actual time=10.189..10.189 rows=89836 loops=1)
              Index Cond: (b = 77)
Planning Time: 0.099 ms
Execution Time: 28.054 ms
```

Veja que a pesquisa foi feita utilizando os dois index em paralelo.


### Retornando a coluna sem index, com uma pesquisa pelas duas colunas com index usando OR

- query
```sql
explain analyze select c from composto where a = 77 or b = 88;
```

- resultado
```sql
Bitmap Heap Scan on composto  (cost=1966.86..53252.87 rows=174943 width=4) (actual time=27.481..304.090 rows=179522 loops=1)
  Recheck Cond: ((a = 77) OR (b = 88))
  Heap Blocks: exact=47459
  ->  BitmapOr  (cost=1966.86..1966.86 rows=175801 width=0) (actual time=17.526..17.528 rows=0 loops=1)
        ->  Bitmap Index Scan on index_a  (cost=0.00..939.69 rows=87901 width=0) (actual time=9.800..9.801 rows=89988 loops=1)
              Index Cond: (a = 77)
        ->  Bitmap Index Scan on index_b  (cost=0.00..939.69 rows=87901 width=0) (actual time=7.724..7.724 rows=90458 loops=1)
              Index Cond: (b = 88)
Planning Time: 0.071 ms
Execution Time: 313.580 ms
```

Veja que a pesquisa é feita em paralelo pelos dois index, mas demora um pouco mais, pois OR sempre retorna mais linhas para analisar do que AND, para ser mais exato, no caso do AND tinhamos 1126 linhas e no do OR 175801 linhas.


### Agora vou criar um index composto e deletar os dois existentes

O index composto será feito entre a e b

```sql
create index index_a_b on composto (a, b);
drop index index_a;
drop index index_b;
```

### Buscando pelas duas colunas do index composto e retornando a coluna fora do index

- query
```sql
explain analyze select c from composto where a = 78 and b = 44;
```

- resultado
```sql
Bitmap Heap Scan on composto  (cost=13.12..3054.69 rows=847 width=4) (actual time=0.253..4.585 rows=891 loops=1)
  Recheck Cond: ((a = 78) AND (b = 44))
  Heap Blocks: exact=884
  ->  Bitmap Index Scan on index_a_b  (cost=0.00..12.91 rows=847 width=0) (actual time=0.125..0.125 rows=891 loops=1)
        Index Cond: ((a = 78) AND (b = 44))
Planning Time: 0.233 ms
Execution Time: 4.691 ms
```

Veja que agora a busca foi feita somente pelo index composto, e a busca foi do tipo `Bitmap Index Scan`

### Buscando pelas duas colunas do index composto e retornando as mesmas colunas


- query
```sql
explain analyze select a,b from composto where a = 11 and b = 23;
```

- resultado
```sql
Index Only Scan using index_a_b on composto  (cost=0.43..546.63 rows=912 width=8) (actual time=0.085..0.398 rows=878 loops=1)
  Index Cond: ((a = 11) AND (b = 23))
  Heap Fetches: 118
Planning Time: 0.101 ms
Execution Time: 0.479 ms
```

Veja que agora a busca foi do tipo `Index Only Scan`.

### Buscando somente por um dos componentes do index composto

- query
```sql
explain analyze select c from composto where b = 36;
```

- Resultado
```sql
Gather  (cost=1000.00..105134.00 rows=86100 width=4) (actual time=9.051..485.700 rows=89884 loops=1)
  Workers Planned: 2
  Workers Launched: 2
  ->  Parallel Seq Scan on composto  (cost=0.00..95524.00 rows=35875 width=4) (actual time=6.501..416.762 rows=29961 loops=3)
        Filter: (b = 36)
        Rows Removed by Filter: 2970039
Planning Time: 0.085 ms
JIT:
  Functions: 12
  Options: Inlining false, Optimization false, Expressions true, Deforming true
  Timing: Generation 1.995 ms, Inlining 0.000 ms, Optimization 4.948 ms, Emission 13.961 ms, Total 20.904 ms
Execution Time: 493.065 ms
```

Veja que o index composto não foi utilizado. Agora vamos fazer a mesma query, mas buscando por a

- query
```sql
explain analyze select c from composto where a = 64;
```

- resultado
```sql
Bitmap Heap Scan on composto  (cost=1106.38..50977.89 rows=97800 width=4) (actual time=24.705..246.541 rows=90390 loops=1)
  Recheck Cond: (a = 64)
  Heap Blocks: exact=41049
  ->  Bitmap Index Scan on index_a_b  (cost=0.00..1081.93 rows=97800 width=0) (actual time=16.680..16.680 rows=90390 loops=1)
        Index Cond: (a = 64)
Planning Time: 0.078 ms
Execution Time: 251.748 ms
```

Veja que agora o index composto foi utilizado. Isso acontece porque na hora que o index foi criado o a veio antes do b `create index index_a_b on composto (a, b);` e essa ordem importa. A coluna a esquerda tem prioridade sob o index.

> OBS.: Caso queira utilizar OR, é possível que o postgres faça um scan na tabela toda, e nem use o index

## 2.9 Observações

- Você deve ter notado que a melhor query é aquela que retorna somente o que está no index, e a cada coluna que você trouxer a mais, vai ficar mais e mais pesada, logo `select * from` é o pior caso possível de retorno.

- Caso precise retornar múltiplas colunas, considere criar index compostos.

- Caso tenha um index composto, considere fazer um index específico para as colunas a direita do index, caso vá fazer queries por elas.

# Referência

- [Database ACID - Live Stream (by Hussein Nasser)](https://www.youtube.com/watch?v=QCKZ3VZ87Qo)

- [Database Indexing Explained (with PostgreSQL)](https://www.youtube.com/watch?v=-qNSXK7s7_w&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=20)

